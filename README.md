# Regras #

Sempre fazer testes antes de desenvolver. O projeto deve estar sempre com 100% de cobertura de testes;
Sempre utilizar os princípios de S.O.L.I.D.

# O que deve ser implementado? #

## Calcular a pontuação de um jogo de boliche ##

O jogador tem 10 jogadas ao todo.

Cada jogada o jogador tem 2 chances de derrubar os 10 pinos. Os pontos são os números de pinos derrubados, mais bônus caso faça “strike” ou “spare”.

STRIKE: Consiste em derrubar todos os pinos na primeira tentativa. O bônus é que os pontos são dobrados nas próximas duas jogadas. Caso seja a ultima jogada o jogador ganha direito a mais dois lances.

SPARE: Consiste em derrubar os 10 pinos na segunda tentativa. O bônus é o número de pinos derrubados na próxima jogada. Caso seja a ultima jogada o jogador ganha mais um lance.