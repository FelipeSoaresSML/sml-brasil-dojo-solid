﻿using System;
using System.Linq;
using DojoSOLID.Interfaces;
using System.Collections.Generic;

namespace DojoSOLID.Aplicacoes
{
    public class Boliche : IBoliche
    {
        public int Pontuacao { get; private set; }

        private List<int> Jogadas;
        public Boliche()
        {
            Jogadas = new List<int>();
        }
        public void Lance(int pinos)
        {
            SalvaPontos(pinos);

            Jogadas.Add(pinos);
        }

        private void SalvaPontos(int pinos)
        {
            Pontuacao += EhPontuacaoDobrada() ? pinos * 2 : pinos;
        }

        private bool EhPontuacaoDobrada()
        {
            if (Jogadas.Count < 1)
                return false;

            var ultimaJogada = Jogadas.Last();

            if (Jogadas.Count < 2)
                return EhStrike(ultimaJogada);

            var indexUltimaJogada = Jogadas.IndexOf(ultimaJogada) - 1;
            var penultimaJogada = Jogadas[indexUltimaJogada];

            return EhStrike(ultimaJogada) || EhStrike(penultimaJogada);
        }

        private bool EhStrike(int pinos)
            => pinos == 10;

    }
}
