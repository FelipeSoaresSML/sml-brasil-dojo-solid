﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DojoSOLID.Interfaces;
using DojoSOLID.Aplicacoes;

namespace DojoSOLID.Test
{
    /// <summary>
    /// Summary description for JogoCompleto
    /// </summary>
    [TestClass]
    public class JogoCompleto
    {
        private IBoliche boliche;

        public JogoCompleto()
        {
            boliche = new Boliche();
        }

        [TestMethod]
        public void JogoSimplesSemSpareOuStrike()
        {
            var random = new Random();
            int totalPontuacao = 0;
            int maxJogada = 9;

            //jogadas
            for (int i = 0; i <= 10; i++)
            {
                int primeiraJogada = 0;
                int segundaJogada = 0;

                primeiraJogada = random.Next(0, maxJogada);
                boliche.Lance(primeiraJogada);

                segundaJogada = random.Next(0, maxJogada - primeiraJogada);
                boliche.Lance(segundaJogada);

                totalPontuacao += primeiraJogada + segundaJogada;
            }

            boliche.Pontuacao.Should().Be(totalPontuacao);
        }

        [TestMethod]
        public void JogadaComStrike()
        {
            var random = new Random();
            int totalPontuacao = 0;
            int maxJogada = 9;

            //jogadas
            List<int> pinosDerrubados = new List<int>() { 10, 2 };

            pinosDerrubados.ForEach(jogada => boliche.Lance(jogada));

            boliche.Pontuacao.Should().Be(14);
        }


    }
}
