﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DojoSOLID.Interfaces
{
    public interface IBoliche
    {
        int Pontuacao { get; }

        void Lance(int pinos);
    }
}
